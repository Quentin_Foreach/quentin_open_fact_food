import {FormControl} from '@angular/forms';

export class Utilisateur {
  constructor(private email: string, private pseudo: string){
  }


  getPseudo(): string {
    return this.pseudo;
  }

  getEmail(): string {
    return this.email;
  }
}
