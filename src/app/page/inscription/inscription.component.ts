import {Component, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {IdentifiantService} from '../../service/identifiant.service';
import {Utilisateur} from '../../interface/utilisateur';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})

export class InscriptionComponent {
  constructor(private router: Router, private identifiant: IdentifiantService) {
  }

  inscription = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    pseudo: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  valideInscription() {
    if (this.inscription.valid) {
      const utilisateur = new Utilisateur(this.inscription.value.email, this.inscription.value.pseudo);
      this.identifiant.saveUser(utilisateur);
      this.router.navigate(['validation']);
    }
  }
}






