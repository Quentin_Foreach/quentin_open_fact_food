import {Component, OnInit} from '@angular/core';
import {IdentifiantService} from '../../service/identifiant.service';
import {Utilisateur} from '../../interface/utilisateur';

@Component({
  selector: 'app-valide',
  templateUrl: './valide.component.html',
  styleUrls: ['./valide.component.scss']
})
export class ValideComponent {

  user: Utilisateur;

  constructor(private identification: IdentifiantService) {
    this.user = this.identification.getCurrentUser();
  }

}
