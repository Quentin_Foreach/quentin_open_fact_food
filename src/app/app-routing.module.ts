import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InscriptionComponent} from './page/inscription/inscription.component';
import {ValideComponent} from './page/valide/valide.component';
import {ScanComponent} from './page/scan/scan.component';
import {DemoComponent} from './page/demo/demo.component';

const routes: Routes = [
  {path:'inscription', component: InscriptionComponent},
  {path: 'validation', component: ValideComponent},
  {path: 'scan', component: ScanComponent},
  {path: 'demo', component: DemoComponent},
  {path: '**', component: InscriptionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
