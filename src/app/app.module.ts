import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderComponent} from './shared/header/header.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {InscriptionComponent} from './page/inscription/inscription.component';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {ValideComponent} from './page/valide/valide.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ScanComponent } from './page/scan/scan.component';
import {BarecodeScannerLivestreamModule, BarecodeScannerLivestreamOverlayModule} from 'ngx-barcode-scanner';
import { DemoComponent } from './page/demo/demo.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    InscriptionComponent,
    ValideComponent,
    ScanComponent,
    DemoComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    BarecodeScannerLivestreamModule,
    BarecodeScannerLivestreamOverlayModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
