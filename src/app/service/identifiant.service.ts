import {Injectable} from '@angular/core';
import {Utilisateur} from '../interface/utilisateur';
import {InscriptionComponent} from '../page/inscription/inscription.component';

@Injectable({
  providedIn: 'root'
})
export class IdentifiantService {

  private user: Utilisateur;

  constructor() {
  }

  saveUser(inscription: Utilisateur) {
    this.user = inscription;
  }

  getCurrentUser(): Utilisateur {
    return this.user;
  }
}
