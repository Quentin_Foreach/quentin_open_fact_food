import { TestBed } from '@angular/core/testing';

import { IdentifiantService } from './identifiant.service';

describe('IdentifiantService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IdentifiantService = TestBed.get(IdentifiantService);
    expect(service).toBeTruthy();
  });
});
